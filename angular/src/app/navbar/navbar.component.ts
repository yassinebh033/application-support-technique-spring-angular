import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  connect : any;
  constructor(public auth : AuthService) { 
    this.connect = this.auth.connectedUser;
  }

  ngOnInit() {
    this.connect = this.auth.connectedUser;
    console.log(this.connect);
  }



  loggout(){

    return this.auth.logOut();
    
  }
  logged(){
    return this.auth.loggedIn();
  }

}
