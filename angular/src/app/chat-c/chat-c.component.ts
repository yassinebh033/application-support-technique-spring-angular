import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ChatService } from '../chat.service';
import { AuthService } from '../auth.service';
import { WebsocketService } from '../websocket.service';

@Component({
  selector: 'app-chat-c',
  templateUrl: './chat-c.component.html',
  styleUrls: ['./chat-c.component.css']
})
export class ChatCComponent implements OnInit {

  listeUsers: any;
  listeMessages: any;
  messageForm: FormGroup;
  chosenUser: any;
  conversation;
  stompClient: any;
  connectUser:any;
  slice: any;
  constructor(public chatService: ChatService, public userService: AuthService, private webSocketService: WebsocketService) {
    this.stompClient = this.webSocketService.connect();
    this.listeUsers = [];
    this.messageForm = new FormGroup({
      content: new FormControl(''),
      user: new FormControl(''),
    });
        this.connectUser=this.userService.connectedUser;

      this.userService.getByusername("conseiller")
      .subscribe((res: any) => {
        console.log(res)

        this.chosenUser = res;
        this.clickUser(this.chosenUser.id_user);
      });
    
    this.stompClient.connect({}, frame => {

      this.stompClient.subscribe('/chat/sendDone', notifications => {
        this.clickUser(this.chosenUser.id_user);
          });
      });
  }

  ngOnInit() {
     this.connectUser= this.userService.connectedUser;
    console.log(this.connectUser)

      this.messageForm = new FormGroup({
        content: new FormControl(''),
        user: new FormControl(this.connectUser.id_user),
      });
      this.userService.getByusername("conseiller")
      .subscribe((res: any) => {
        console.log(res)
        this.chosenUser = res;
        this.clickUser(this.chosenUser.id_user);
      });
    

  }
  clickUser(idUser) {
    this.chosenUser.id_user = idUser;
    this.chatService.getConversation(this.chosenUser.id_user,this.connectUser.id_user )
    .subscribe( (res: any) => {
      console.log(res);
      this.conversation = res.id_conv;
      console.log(res.messages);

      this.listeMessages = res.messages.sort((a,b) => {
        return a.id_msg - b.id_msg;
      });;
        
      console.log(this.listeMessages);


    });
  }
  sendMessage() {
    this.chatService.sendMessage(this.messageForm.value.content, this.messageForm.value.user, this.conversation)
    .subscribe((res: any) => {
      console.log(res);
    });
 }


}











 

