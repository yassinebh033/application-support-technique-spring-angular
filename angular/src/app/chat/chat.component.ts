import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ChatService } from '../chat.service';
import { AuthService } from '../auth.service';
import { WebsocketService } from '../websocket.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  listeUsers: any;
  listeMessages: any;
  messageForm: FormGroup;
  chosenUser: any;
  conversation;
  stompClient: any;
  connectUser:any;
  constructor(public chatService: ChatService, public userService: AuthService, private webSocketService: WebsocketService) {
    this.stompClient = this.webSocketService.connect();
    this.listeMessages = [];
    this.listeUsers = [];
    this.messageForm = new FormGroup({
      content: new FormControl(''),
      user: new FormControl(''),
    });
        this.connectUser=this.userService.connectedUser;

      this.userService.getAllClient()
      .subscribe((res: any) => {
        console.log(res)

        this.listeUsers = res.filter(obj => obj.username !== this.userService.connectedUser);
        this.clickUser(this.listeUsers[0].id_user);
      });
    
    this.stompClient.connect({}, frame => {

      this.stompClient.subscribe('/chat/sendDone', notifications => {
        this.clickUser(this.chosenUser);
          });
      });
  }

  ngOnInit() {
     this.connectUser= this.userService.connectedUser;
    console.log(this.connectUser)

      this.messageForm = new FormGroup({
        content: new FormControl(''),
        user: new FormControl(this.connectUser.id_user),
      });
      this.userService.getAllClient()
      .subscribe((res: any) => {
        console.log(res)
        this.listeUsers = res.filter(obj => obj.username !== this.userService.connectedUser);
        this.clickUser(this.listeUsers[0].id_user);
      });
    

  }
  clickUser(idUser) {
    this.chosenUser = idUser;
    this.chatService.getConversation(this.connectUser.id_user, this.chosenUser )
    .subscribe( (res: any) => {
      console.log(res);
      this.conversation = res.id_conv;
      this.listeMessages =       this.listeMessages = res.messages.sort((a,b) => {
        return a.id_msg - b.id_msg;
      });;

      console.log(this.listeMessages);
    });
  }
  sendMessage() {
    this.chatService.sendMessage(this.messageForm.value.content, this.messageForm.value.user, this.conversation)
    .subscribe((res: any) => {
      console.log(res);
    });
 }
}
 

