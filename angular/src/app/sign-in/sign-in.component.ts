import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { stringify } from 'querystring';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  client = new User();
  constructor(private auth : AuthService, private router : Router) { }

  SignForm: FormGroup;

  ngOnInit() {
    this.SignForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  get username() {
    return this.SignForm.get('username');
  }

  get password() {
    return this.SignForm.get('password');
  }

  sign(){
  
    this.client.username=this.SignForm.get('username').value;
    this.client.password=this.SignForm.get('password').value;
    console.log(this.client);
    this.auth.login(this.client)
    .subscribe(
      (res: any) => {
        localStorage.setItem('token', res.token);
       
        console.log(res);
        
        this.auth.getByusername(this.SignForm.get('username').value)
        .subscribe(
          (res: any) => {
            localStorage.setItem('loggedUser', JSON.stringify(res));
            this.auth.connectedUser = res;
            
            console.log ("user est: ",this.auth.connectedUser);
           
            if (this.auth.connectedUser.role=="Client"){
            this.router.navigate(['../home']);
          }
          else if (this.auth.connectedUser.role=="admin" || this.auth.connectedUser.role=="Chef-Back" || this.auth.connectedUser.role=="Chef-Front"){
            this.router.navigate(['../config']);
          }
          else if (this.auth.connectedUser.role=="Conseiller"){
            this.router.navigate(['../chat']);
          }
          else{
               this.router.navigate(['../admin']);
}
          },
          err => {
            console.log ("erreur connecteur");
            this.router.navigate(['../signIn']);
          }
        )  
      },
      err => {
        console.log ("erreur");
      }
    )
}
}
