import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpBackend} from '@angular/common/http'
import {Observable} from 'rxjs'
import { Router } from '@angular/router';
import { Feedback } from './feedback';
import { AuthService } from './auth.service';


const httpOptions={ headers :new HttpHeaders().append( 'Authorization' , `Bearer ${localStorage.getItem('token')}` ) };


@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
   

  reclamations : any;
  constructor(private http: HttpClient, private _router: Router, private handler : HttpBackend, private auth :AuthService) { 
    this.http=new HttpClient(this.handler); 
  }
     apiUrl="http://localhost:5000/reclamation/addReclamation/"+this.auth.connectedUser.id_user;
     apiUrl1="http://localhost:5000/reclamation/delete/"+this.auth.connectedUser.id_user;
     
addReclamation(feedback : Feedback ): Observable<Feedback>{
  return this.http.post<Feedback>(this.apiUrl,feedback,httpOptions);
}

supReclamation(id_rec : any ): Observable<Feedback>{
  return this.http.post<Feedback>(this.apiUrl1,id_rec,httpOptions);
}

getAll(){
  const apiUrl2="http://localhost:5000/reclamation/all";

  return this.http.get(apiUrl2,httpOptions);
}

getAllByUser(id_user:any): Observable<any>{
  const apiUrl5="http://localhost:5000/reclamation/allByClient"

  return this.http.post(apiUrl5,id_user,httpOptions);
}

affectationTech_back( id_rec: any , id_tback:any){
  console.log(id_tback,id_rec)
  const apiUrl3="http://localhost:5000/reclamation/affectationTech_back/"+id_tback+"/"+id_rec;
 
  return this.http.get(apiUrl3,httpOptions);
}

affectationTech_Front(id_rec : any,id_tfront:any ){
  console.log(id_tfront,id_rec)

 const apiUrl4="http://localhost:5000/reclamation/affectationTech_Front/"+id_tfront+"/"+id_rec;

  return this.http.get(apiUrl4,httpOptions);
}
getAllFront(){
 const apiUrl6="http://localhost:5000/reclamation/AllBytype/Front_end";

  return this.http.get(apiUrl6,httpOptions);
}
getAllback(){
 const apiUrl7="http://localhost:5000/reclamation/AllBytype/Back_end";

  return this.http.get(apiUrl7,httpOptions);
}

affectReponse(id_rec : any, reponse:String ): Observable<String>{
  const apiUrl9="http://localhost:5000/reclamation/reponse/"+id_rec;
  return this.http.post<String>(apiUrl9,reponse,httpOptions);

}

}