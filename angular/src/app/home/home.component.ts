import { Component, OnInit } from '@angular/core';
import {  FormControl } from '@angular/forms';
import { Feedback } from '../feedback';
import { FeedbackService } from '../feedback.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
feedback=new Feedback();
id: any;
  type= new FormControl('');
  niveau= new FormControl('');
description=new FormControl('');
  
constructor(private feedbackServ : FeedbackService,  private router : Router, private auth : AuthService){}
  ngOnInit() {
    this.auth.id_user=this.auth.connectedUser.id_user;
    console.log(this.auth.id_user);
  
}

reclamer(){

  this.feedback.type=this.type.value;
  this.feedback.etat="Initial";
  this.feedback.niveau=this.niveau.value;
  this.feedback.description=this.description.value;
  console.log(this.feedback);
this.feedbackServ.addReclamation(this.feedback)

.subscribe(
  (res: any) => {
    console.log("add réclamation");
    console.log(this.auth.id_user);
    this.router.navigate(['../home']);
    
  },
  err => {
    console.log ("erreur");
    console.log(this.auth.id_user);
    this.router.navigate(['../home']);

  }
)  }



supprimerReclamation() {
  this.feedback.id_rec=this.id;
  this.feedbackServ.supReclamation(this.feedback.id_rec)
  .subscribe(
    (res: any) => {
      console.log("remove réclamation");
       
    },
    err => {
      console.log ("erreur");
      this.router.navigate(['/home']);
    }
  )  }

}



