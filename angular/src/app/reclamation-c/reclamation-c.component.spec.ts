import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReclamationCComponent } from './reclamation-c.component';

describe('ReclamationCComponent', () => {
  let component: ReclamationCComponent;
  let fixture: ComponentFixture<ReclamationCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReclamationCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReclamationCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
