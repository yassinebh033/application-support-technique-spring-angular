import { Component, OnInit } from '@angular/core';
import { FeedbackService } from '../feedback.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-reclamation-c',
  templateUrl: './reclamation-c.component.html',
  styleUrls: ['./reclamation-c.component.css']
})
export class ReclamationCComponent implements OnInit {
  constructor(private feedServ : FeedbackService,private auth:AuthService) { }
reclamations:any
  ngOnInit() {
    this.auth.connectedUser;
console.log(this.auth.connectedUser.id_user);
    this.hetRec();
  }
hetRec(){
  this.feedServ.getAllByUser(this.auth.connectedUser.id_user)
  
  .subscribe(
    (res: any) => {
      this.reclamations=res;
      console.log( this.reclamations);
    },
    err => {
      console.log ("erreur");
    }
  )  }
}

