import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../user';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  connect : User;
  password= new FormControl('');
  constructor( private auth : AuthService) { }
  imageUrl: string = "/assets/images/default-avatar.png";

  ngOnInit() {
    this.connect = this.auth.connectedUser;
    console.log(this.connect);

  }
  modifier(){
    this.connect.password=this.password.value;
console.log(this.connect);
  }
}
