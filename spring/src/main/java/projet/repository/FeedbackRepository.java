package projet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import projet.entity.Feedback;

@Repository("repositoryFeddback")
public interface FeedbackRepository extends JpaRepository<Feedback, Integer>{

}
