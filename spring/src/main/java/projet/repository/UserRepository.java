package projet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import projet.entity.User;
@Repository("repositoryUser")
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}
