package projet.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import projet.entity.Conversation;
import projet.entity.User;
import projet.repository.ConversationRepository;
import projet.services.ConversationService;

@Service("conversationservice")
public class ConversationServiceImpl implements ConversationService {
	@Autowired
	ConversationRepository conversationrepository;
	
	@PersistenceContext
	EntityManager em;

	@Override
	public Conversation getOneConversation(Integer id_cons, Integer id_cli) throws NoResultException {
		List<Conversation> conv = null;
		TypedQuery<Conversation> query = (TypedQuery<Conversation>) em.createQuery("FROM Conversation u   WHERE u.conseiller.id_user = :conseiller and u.client.id_user= :client" ,Conversation.class);
		conv = query.setParameter("conseiller", id_cons ).setParameter("client",id_cli).getResultList();
		if(conv.isEmpty() != true) {
			return conv.get(0);
		} else {
			/*TypedQuery<Conversation> query2 = (TypedQuery<Conversation>) em.createQuery("FROM Conversation u WHERE u.conseiller.id_user = :conseiller and u.client.id_user = :client" ,Conversation.class);
			conv = query2.setParameter("conseiller", id_cons).setParameter("client", id_cli).getResultList();
			if(conv.isEmpty() != true) {
				return conv.get(0);
			} else {*/
				Conversation newConv = new Conversation();
				User u1 = new User();
				u1.setId_user(id_cons);
				User u2 = new User();
				u2.setId_user(id_cli);
				newConv.setConseiller(u1);
				newConv.setClient(u2);
				conversationrepository.save(newConv);
				TypedQuery<Conversation> query3 = (TypedQuery<Conversation>) em.createQuery("FROM Conversation	 u  WHERE u.conseiller.id_user = :conseiller and u.client.id_user = :client  " ,Conversation.class);
				conv = query3.setParameter("conseiller", id_cons).setParameter("client", id_cli).getResultList();
				return conv.get(0);
			//}
		}
	}
}
