package projet.services;

import javax.persistence.NoResultException;

import projet.entity.Conversation;

public interface ConversationService {
	public Conversation getOneConversation(Integer user1, Integer user2) throws NoResultException;

}
