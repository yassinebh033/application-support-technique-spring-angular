package projet.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import projet.entity.Feedback;
import projet.entity.User;
import projet.repository.FeedbackRepository;

@Service("FeedbackService")
public class FeedbackServiceImpl implements FeedbackService{

	@Autowired
	FeedbackRepository feedbackrepo;
	@PersistenceContext
	EntityManager em;
	@Autowired
	UserService userService;
	@Autowired
    JavaMailSender emailSender;
	

	@Override
	public void ajouterFeedback(Feedback feedback, int id_client) {
		User client = userService.getUserById(id_client);
           feedback.setClient(client); 
		if (feedback.getType().equals("Front_end")) {
			
			User chef_front=userService.getByRole("Chef-Front");
			feedback.setChef_front(chef_front);	

		}
		else {
			User chef_back= userService.getByRole("Chef-Back");
			feedback.setChef_back(chef_back);
		}
		
		feedbackrepo.save(feedback);

		}
	
	@Override
	public Feedback getFeedbackByType(String type) {
		TypedQuery<Feedback> query = (TypedQuery<Feedback>) em.createQuery("SELECT f FROM Feedback f WHERE f.type = :type" ,Feedback.class);
		Feedback f=query.setParameter("type", type).getSingleResult();
			return  f;
	}
	
	@Override
	public List<Feedback> getAllFeedbackByType(String type) {
		TypedQuery<Feedback> query = (TypedQuery<Feedback>) em.createQuery("SELECT f FROM Feedback f WHERE f.type = :type" ,Feedback.class);
		List<Feedback> f=query.setParameter("type", type).getResultList();
			return  f;
	}
	
	@Override
	public  List<Feedback> getallFeedbackByIdClient(int id_user) {
		TypedQuery<Feedback> query = (TypedQuery<Feedback>) em.createQuery("SELECT f FROM Feedback f WHERE f.client.id_user = :type" ,Feedback.class);
		List<Feedback> f=query.setParameter("type", id_user).getResultList();
			return  f;
	}
	@Override
	public  List<Feedback> getallFeedbackByIdChef_back(int id_user){
		TypedQuery<Feedback> query = (TypedQuery<Feedback>) em.createQuery("SELECT f FROM Feedback f WHERE f.chef_back.id_user = :type" ,Feedback.class);
		List<Feedback> f=query.setParameter("type", id_user).getResultList();
			return  f;
	}
	@Override
	public  List<Feedback> getallFeedbackByIdChef_front(int id_user){
		TypedQuery<Feedback> query = (TypedQuery<Feedback>) em.createQuery("SELECT f FROM Feedback f WHERE f.chef_front.id_user = :type" ,Feedback.class);
		List<Feedback> f=query.setParameter("type", id_user).getResultList();
			return  f;
	}
	@Override
	public  List<Feedback> getallFeedbackByIdTECH_back(int id_user){
		TypedQuery<Feedback> query = (TypedQuery<Feedback>) em.createQuery("SELECT f FROM Feedback f WHERE f.tech_back.id_user = :type" ,Feedback.class);
		List<Feedback> f=query.setParameter("type", id_user).getResultList();
			return  f;
	}
	@Override
	public  List<Feedback> getallFeedbackByIdTech_front(int id_user){
		TypedQuery<Feedback> query = (TypedQuery<Feedback>) em.createQuery("SELECT f FROM Feedback f WHERE f.tech_front.id_user = :type" ,Feedback.class);
		List<Feedback> f=query.setParameter("type", id_user).getResultList();
			return  f;
	}
	
	@Override
	public void supprimerFeedbackById(int id_rec) {
           Feedback feedback= em.find(Feedback.class, id_rec);
           em.remove(feedback);
	}

	@Override
	public List<Feedback> getallFeedback() {
		
		return feedbackrepo.findAll();
	}

	@Override
	public Feedback getFeedbackById(int id_rec) {
		return feedbackrepo.getOne(id_rec);
	}

	@Override
	public void affeTech_back (int idtech_back, int id_rec) {
		User tech_back= em.find(User.class, idtech_back);
		Feedback feedback= em.find(Feedback.class, id_rec);
		             feedback.setTech_back(tech_back); 
		             feedback.setEtat("En cours");
		     		feedbackrepo.save(feedback);
	}
	
	@Override
	public void affeTech_front (int idtech_front, int id_rec) {
		User tech_front= em.find(User.class, idtech_front);
		Feedback feedback= em.find(Feedback.class, id_rec);
		         feedback.setTech_front(tech_front); 
		         feedback.setEtat("En cours");
		 		feedbackrepo.save(feedback);
			}

    public void ajoutReponse(String reponse, int id_rec) {
    	Feedback feedback=em.find(Feedback.class, id_rec);
    	feedback.setReponse(reponse);
    	feedback.setEtat("Traitee");
    	User client=feedback.getClient();
    	SimpleMailMessage message= new SimpleMailMessage(); 

    	message.setTo(client.getEmail());
    	message.setSubject("Traitement de votre reclamation");
    	message.setText("bonjour, votre reclamation a ete bien traitee");
    	emailSender.send(message);
		feedbackrepo.save(feedback);
    }
	
	
}
