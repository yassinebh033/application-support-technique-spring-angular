package projet.entity;


import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;

import projet.config.BCryptManagerUtil;
import projet.entity.Message;
import projet.entity.Conversation;

@Entity
public class User  {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_user;
    private String nom;
    private String prenom;
    @Column(name = "username", unique = true)
    private String username;
    private String password;
    private int tel;
    @Email
    private String email;
    private String role;
    @OneToMany(mappedBy="conseiller")
	@JsonIgnore
	private List<Conversation> conversationsConseiller = new ArrayList<Conversation>();
	@OneToOne(mappedBy="client")
	@JsonIgnore
	private Conversation conversationsClient;
	@OneToMany(mappedBy="user")
	@JsonIgnore
	private List<Message> message = new ArrayList<Message>();
	@OneToMany(mappedBy="admin") 
	private List<Feedback> feedbacksA;
	@OneToMany(mappedBy="client") 
	private List<Feedback> feedbacksC;
	@OneToMany(mappedBy="chef_back") 
	private List<Feedback> feedbacksChef_B;
	@OneToMany(mappedBy="chef_front") 
	private List<Feedback> feedbacksChef_F;
	@OneToMany(mappedBy="tech_back") 
	private List<Feedback> feedbacksTech_B;
	@OneToMany(mappedBy="tech_front") 
	private List<Feedback> feedbacksTech_F;

public User() {

}
public User(String username, String password, String adresse) {
	super();
	this.username = username;
	this.password = BCryptManagerUtil.passwordEncoder().encode(password);
}

public User(int id_user, String username, String password) {
	super();
	this.id_user = id_user;
	this.username = username;
    this.password = BCryptManagerUtil.passwordEncoder().encode(password);
}
  
	 public List<Conversation> getConversationsConseiller() {
			return conversationsConseiller;
		}


		public void setConversationsConseiller(List<Conversation> conversationsConseiller) {
			this.conversationsConseiller = conversationsConseiller;
		}


		public Conversation getConversationsClient() {
			return conversationsClient;
		}


		public void setConversationsClient(Conversation conversationsClient) {
			this.conversationsClient = conversationsClient;
		}


		public List<Message> getMessage() {
			return message;
		}


		public void setMessage(List<Message> message) {
			this.message = message;
		}
	
		
		

	@Override
	public String toString() {
		return "{\"id\":" + id_user + ", \"username\":" + username + "}";
	}
	//chat


@JsonIgnore
public List<Feedback> getFeedbacksC() {
	return feedbacksC;
}


public void setFeedbacksC(List<Feedback> feedbacksC) {
	this.feedbacksC = feedbacksC;
}

@JsonIgnore
public List<Feedback> getFeedbacksChef_B() {
	return feedbacksChef_B;
}


public void setFeedbacksChef_B(List<Feedback> feedbacksChef_B) {
	this.feedbacksChef_B = feedbacksChef_B;
}

@JsonIgnore
public List<Feedback> getFeedbacksChef_F() {
	return feedbacksChef_F;
}


public void setFeedbacksChef_F(List<Feedback> feedbacksChef_F) {
	this.feedbacksChef_F = feedbacksChef_F;
}

@JsonIgnore
public List<Feedback> getFeedbacksTech_B() {
	return feedbacksTech_B;
}

public void setFeedbacksTech_B(List<Feedback> feedbacksTech_B) {
	this.feedbacksTech_B = feedbacksTech_B;
}

@JsonIgnore
public List<Feedback> getFeedbacksTech_F() {
	return feedbacksTech_F;
}

public void setFeedbacksTech_F(List<Feedback> feedbacksTech_F) {
	this.feedbacksTech_F = feedbacksTech_F;
}

public List<Feedback> getFeedbacksA() {
	return feedbacksA;
}


public void setFeedbacksA(List<Feedback> feedbacksA) {
	this.feedbacksA = feedbacksA;
}

public String getRole() {
	return role;
}

public void setRole(String role) {
	this.role = role;
}

public int getId_user() {
	return id_user;
}

public void setId_user(int id_user) {
	this.id_user = id_user;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public String getPrenom() {
	return prenom;
}

public void setPrenom(String prenom) {
	this.prenom = prenom;
}

public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password  = BCryptManagerUtil.passwordEncoder().encode(password);
}

public int getTel() {
	return tel;
}


public void setTel(int tel) {
	this.tel = tel;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}


}
