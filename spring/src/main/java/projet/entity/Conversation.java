package projet.entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import projet.entity.Message;
import projet.entity.User;

@Entity
public class Conversation {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id_conv;

	
	@ManyToOne
	private User conseiller;
	@OneToOne
	private User client;
	
	@OneToMany(mappedBy="conversation")
	private List<Message> messages = new ArrayList<Message>();
	
	
	public Conversation() {
	}

	
	
	public Conversation(int id_conv, User conseiller, User client, List<Message> messages) {
		super();
		this.id_conv = id_conv;
		this.conseiller = conseiller;
		this.client = client;
		this.messages = messages;
	}



	public User getConseiller() {
		return conseiller;
	}



	public void setConseiller(User conseiller) {
		this.conseiller = conseiller;
	}



	public User getClient() {
		return client;
	}



	public void setClient(User client) {
		this.client = client;
	}



	public List<Message> getMessages() {
		return messages;
	}



	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}



	public int getId_conv() {
		return id_conv;
	}

	public void setId_conv(int id_conv) {
		this.id_conv = id_conv;
	}

	
	
	
}

