package projet.entity;

import java.text.DateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import projet.entity.Conversation;
import projet.entity.User;

@Entity
public class Message {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_msg;
	private String content;
	private Date createdDate = new Date(); 
	@ManyToOne
	private User user;
	
	
	
	

	@ManyToOne
	@JsonIgnore
	private Conversation conversation;
	
	public Message() {
	}







	public Message(int id_msg, String content, Date createdDate, User user, Conversation conversation) {
		super();
		this.id_msg = id_msg;
		this.content = content;
		this.createdDate = createdDate;
		this.user = user;
		this.conversation = conversation;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public Conversation getConversation() {
		return conversation;
	}



	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}



	public int getId_msg() {
		return id_msg;
	}
	public void setId_msg(int id_msg) {
		this.id_msg = id_msg;
	}
	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
	}
	

	public Date getCreatedDate() {
		return createdDate;
	}



	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	
	
}
