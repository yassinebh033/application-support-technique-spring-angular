package projet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import projet.services.ConversationService;


@CrossOrigin("*")
@RestController
@RequestMapping("/conversation")

public class ConversationController {
	@Autowired
	ConversationService conversationservice;
	
	@RequestMapping(value = "/getOneConversation/{id_user1}/{i_user2}", method = RequestMethod.GET)
	public ResponseEntity<?> getOneConversation(@PathVariable("id_user1") Integer id_cons, @PathVariable("i_user2") Integer i_clien ) throws Exception {
		return ResponseEntity.ok(conversationservice.getOneConversation(id_cons, i_clien));
	}
}
