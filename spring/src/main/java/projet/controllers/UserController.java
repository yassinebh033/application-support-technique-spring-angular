package projet.controllers;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import projet.config.JwtTokenUtil;
import projet.entity.JwtResponse;
import projet.entity.User;
import projet.repository.UserRepository;
import projet.services.UserService;

@RestController
@CrossOrigin("*")
@RequestMapping("/user")

public class UserController {
	@Autowired
	UserService userService;
	@Autowired
	UserRepository userrepository;
		
		 @Autowired
		 JwtTokenUtil jwtTokenUtil;
	     @Autowired
	     EntityManager em;
	
	 
		   @RequestMapping(value = "/registration", method = RequestMethod.POST)
		    public void ajouterUser(@RequestBody User user) {
			  userrepository.save(user);
	          
	        }
		   @RequestMapping(value = "/modification", method = RequestMethod.POST)
		    public void modifier( @RequestBody User user) {
			   userService.modifier( user);
	          
	        }
		   
		    @RequestMapping(value = "/login", method = RequestMethod.POST)
			public ResponseEntity<?> createAuthenticationToken(@RequestBody User user) throws Exception {
				final User user1 = userService.getByUsername(user.getUsername());
				final String token = jwtTokenUtil.generateToken(user1);
				return ResponseEntity.ok(new JwtResponse(token));
			}
		 
		    
		 
		 @RequestMapping(value = "/delete", method = RequestMethod.POST)
			public void supprimerUserByID(@RequestBody int id_user) {
			 userService.supprimerUserByID(id_user);
			}

		    @RequestMapping(value = "/all", method = RequestMethod.GET)
			public List<User> getallUser(){
				return userService.getallUser();
			}

		    @RequestMapping(value = "/one/{id_user}", method = RequestMethod.POST)
			 public User getUserById(@PathVariable("id_user") int id_user) {
				return userService.getUserById(id_user);
			}

		   
		    @RequestMapping(value = "/username", method = RequestMethod.POST)
		    public User getByUsername(@RequestBody String username) {
	            return userService.getByUsername(username);
		    }
		    
		    @RequestMapping(value = "/role/{role}", method = RequestMethod.GET)
		    public User getUserByRole(@PathVariable("role") String role) {
	           return userService.getByRole(role);
		    }
		   
		    
			
			 
@RequestMapping(value = "/allTech_back", method = RequestMethod.GET)
public List<User> getallTech_back(){
return userService.getAllTech_back();
					    }
			  
@RequestMapping(value = "/allTech_front", method = RequestMethod.GET)
	public List<User> getallTech_front() {
	return userService.getAllTech_front();
			}
				
	@RequestMapping(value = "/allClient", method = RequestMethod.GET)
	public List<User> getallC(){
	return userService.getAllClient();
					    }
	
	
}
