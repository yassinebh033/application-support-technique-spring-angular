package projet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import projet.entity.Conversation;
import projet.entity.Message;
import projet.entity.User;
import projet.repository.ConversationRepository;
import projet.repository.UserRepository;
import projet.services.MessageService;


@CrossOrigin("*")
@RestController
@RequestMapping("/message")

public class MessageController {
	@Autowired
    private SimpMessagingTemplate template;
	@Autowired
	MessageService messageservice;
	@Autowired
	ConversationRepository conversationrepository;
	@Autowired
	UserRepository userrepository;
	@RequestMapping(value = "/sendMessage/{id_user}/{idConv}", method = RequestMethod.POST)
	public ResponseEntity<?> sendMessage(@PathVariable("id_user") Integer id_user, @PathVariable("idConv") Integer idConv, @RequestBody String obj ) throws Exception {
		Message message = new Message();
		User user = new User();
		user.setId_user(id_user);
		Conversation conv = conversationrepository.getOne(idConv);
		message.setUser(user);
		message.setConversation(conv);
		message.setContent(obj);
		messageservice.sendMessage(message);
		template.convertAndSend("/chat/sendDone", "done");
		return ResponseEntity.ok("done");
	}
}
